# DNA Sequence
El proyecto es un API REST que detecte si una persona tiene diferencias genéticas basándose en su secuencia de ADN.

#### Repositorio
```
https://gitlab.com/edmon_1024/dna-sequence
```

#### Información general
Se usó javascript como lenguaje de programación, fastify como framework de desarrollo y firestore como base de datos.
<br/>
Se encuentra la configuración para desplegar la aplicación en vercel.
<br/>

#### Configuración inicial
1.- Se necesita descargar las configuraciones desde el sitio de Firebase
2.- Agregarlas en el archivo de variables de entorno en la siguiente ruta `api/.env` en el campo `FIREBASE_SERVICE_ACCOUNT_KEY`
3.- Instalar dependencias: `nom install`


### Scripts disponibles
En el directorio de "api" se encuentra el proyecto

#### Para ejecutar modo desarrollo se usa:
### `npm run dev`

Se inicia la app con la siguiente dirección.
Abrir [http://localhost:3000](http://localhost:3000).


#### Para ejecutar en modo producción:
### `npm run start`

#### Para correr las pruebas:
### `npm run test`

<br/>

#### Pruebas unitarias 
![Pruebas unitarias y cobertura](/imgs/pruebas_unitarias.png "Pruebas unitarias y cobertura")


