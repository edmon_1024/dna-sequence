'use strict'

// Read the .env file.
const dotenv = require("dotenv")
dotenv.config();

// Require the framework
const Fastify = require('fastify')
const fastify = Fastify({
  logger: true,
});

const path = require('path')
const AutoLoad = require('fastify-autoload')

// Place here your custom code!
fastify.register(AutoLoad, {
  dir: path.join(__dirname, 'custom_plugins'),
})

////fastify.register(require('./plugins/fastify-firebase'), {
////  name: "dna-sequence-api",
////  cert: JSON.parse(process.env.FIREBASE_SERVICE_ACCOUNT_KEY),
////})

// Do not touch the following lines

// This loads all plugins defined in plugins
// those should be support plugins that are reused
// through your application
fastify.register(AutoLoad, {
  dir: path.join(__dirname, 'plugins'),
})

// This loads all plugins defined in routes
// define your routes in one of these
fastify.register(AutoLoad, {
  dir: path.join(__dirname, 'routes'),
  options: Object.assign({
    prefix:"/v1"
  })
})

fastify.listen(3000, function (err, address) {
  if (err) {
    fastify.log.error(err)
    process.exit(1)
  }
  fastify.log.info(`server listening on ${address}`)
})

