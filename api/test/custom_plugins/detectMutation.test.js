'use strict'

const { test } = require('tap')
const Fastify = require('fastify')
const DetectMutation = require('../../custom_plugins/detectMutation')

test('mutation - not array', async (t) => {
  const fastify = Fastify()
  fastify.register(DetectMutation)

  await fastify.ready()
  t.equal(fastify.detectMutation(""), false)
})

test('mutation - array empty', async (t) => {
  const fastify = Fastify()
  fastify.register(DetectMutation)

  await fastify.ready()
  t.equal(fastify.detectMutation([]), false)
})

test('mutation - array unknown', async (t) => {
  const fastify = Fastify()
  fastify.register(DetectMutation)

  await fastify.ready()
  t.equal(fastify.detectMutation([1,2,3,4,5,6,7,8,9]), false)
})

test('mutation - array malformed', async (t) => {
  const fastify = Fastify()
  fastify.register(DetectMutation)

  await fastify.ready()
  t.equal(fastify.detectMutation(["aa", "djwqroer"]), false)
})

test('detect mutation - horizontal', async (t) => {
  const fastify = Fastify()
  fastify.register(DetectMutation)

  await fastify.ready()
  t.equal(fastify.detectMutation([		
    "AAAAGA",
		"CCGTCC",
		"TTATGT",
		"AGAAGG",
		"CCTCTA",
		"TCACTG"
  ]), true)
})

test('detect mutation - vertical', async (t) => {
  const fastify = Fastify()
  fastify.register(DetectMutation)

  await fastify.ready()
  t.equal(fastify.detectMutation([		
    "TACAGA",
		"TCGTCC",
		"TTATGT",
		"TGAAGG",
		"CCTCTA",
		"TCACTG"
  ]), true)
})

test('detect mutation - transversal', async (t) => {
  const fastify = Fastify()
  fastify.register(DetectMutation)

  await fastify.ready()
  t.equal(fastify.detectMutation([		
    "ATGCGA",
		"CAGTCC",
		"TTATGT",
		"AGAAGG",
		"CCTCTA",
		"TCACTG"
  ]), true)
})

