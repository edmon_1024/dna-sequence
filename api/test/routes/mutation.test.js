'use strict'

const { test } = require('tap')
const { build } = require('../helper')

test('mutation success', async (t) => {
  const app = await build(t)

  const res = await app.inject({
    method: 'POST',
    url: '/v1/mutation',
    body: {
	    dna: ["AAAAAA","CCCCCC","TTTTTT","ATCGAT","ATCGAT","AATTCT"]
    }
  })

  t.equal(res.statusCode, 200, "returns a status code of 200")
})

test('mutation - body empty', async (t) => {
  const app = await build(t)

  const res = await app.inject({
    method: 'POST',
    url: '/v1/mutation',
    body: {}
  })

  t.equal(res.statusCode, 400, "status 400")
})

test('mutation - array malformed', async (t) => {
  const app = await build(t)

  const res = await app.inject({
    method: 'POST',
    url: '/v1/mutation',
    body: {
      dna: [1,2,3,4,5,"",null]
    }
  })

  t.equal(res.statusCode, 400, "returns a status code of 400")
})





