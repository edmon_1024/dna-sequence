'use strict'

const { test } = require('tap')
const { build } = require('../helper')

test('stats route', async (t) => {
  const app = await build(t)

  const res = await app.inject({
    url: '/v1/stats'
  })

  //t.same(JSON.parse(res.payload), {
	//  count_mutations: 0,
	//  count_no_mutation: 0,
	//  ratio: 0
  //})

  t.equal(res.statusCode, 200, "returns a status code of 200")
})

