'use strict'

const { test } = require('tap')
const { build } = require('../helper')

test('root route', async (t) => {
  const app = await build(t)

  const res = await app.inject({
    url: '/v1'
  })
  t.equal(res.statusCode, 200, "returns a status code of 200")
})

