'use strict'

// Read the .env file.
const dotenv = require("dotenv")
dotenv.config();
const path = require('path')
const AutoLoad = require('fastify-autoload')

module.exports = async function (fastify, opts) {

  // Place here your custom code!
  fastify.register(AutoLoad, {
    dir: path.join(__dirname, 'custom_plugins'),
    options: Object.assign({}, opts)
  })

  // Do not touch the following lines

  // This loads all plugins defined in plugins
  // those should be support plugins that are reused
  // through your application
  fastify.register(AutoLoad, {
    dir: path.join(__dirname, 'plugins'),
    options: Object.assign({}, opts)
  })

  ////fastify.register(require('./plugins/fastify-firebase'), {
  ////  name: "dna-sequence-api",
  ////  cert: JSON.parse(process.env.FIREBASE_SERVICE_ACCOUNT_KEY),
  ////})

  // This loads all plugins defined in routes
  // define your routes in one of these
  fastify.register(AutoLoad, {
    dir: path.join(__dirname, 'routes'),
    options: Object.assign({
      prefix:"/v1"
    }, opts)
  })
}

