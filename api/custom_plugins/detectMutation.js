'use strict'

const fp = require('fastify-plugin')

/**
 * This plugin adds some utilities to detect dna mutations
 *
 *
 */
module.exports = fp(async function (fastify, opts) {
  fastify.decorate('detectMutation', function (dna=[]) {
    if (
      !Array.isArray(dna) ||
      dna.length !== 6 ||
      dna.filter(x => typeof x === "string" && x.length == 6 && x.match(/[ATCG]{6}/)).length !== 6
    ){
      return false
    }

    const regex = /A{4}|T{4}|C{4}|G{4}/
    let mutationExists = false
    const size = 6

    for (let rowI = 0; rowI < dna.length; rowI++) {
      if (dna[rowI].search(regex) !== -1) {
        mutationExists = true
        break
      }
    }
    
    if (mutationExists) return true

    let items = []
    for (let colI = 0; colI < size; colI++) {
      let item = ""

      for (let rowI = 0; rowI <= size; rowI++) {
        if (rowI < size) {
          item += dna[rowI][colI]
        } else {
          items.push(item)
          break
        }
      }
    }

    for (let rowI = 0; rowI < items.length; rowI++) {
      if (items[rowI].search(regex) !== -1) {
        mutationExists = true
        break
      }
    }

    if (mutationExists) return true

    for (let idx = 0; idx < ((size*2)-1); idx++){
      let [rowI, colI, item] = [idx, 0, ""]

      while (rowI >= 0 && colI < 6){
        if (rowI < 6){
          item += dna[rowI][colI]
        }
        rowI -= 1
        colI += 1
      }

      if (item.length >= 4 && item.search(regex) !== -1) {
        mutationExists = true
        break
      }
    }

    if (mutationExists) return true

    const dnaReverse = dna.map(x => x.split("").reverse().join(""))
    for (let idx = 0; idx < ((size*2)-1); idx++){
      let [rowI, colI, item] = [idx, 0, ""]

      while (rowI >= 0 && colI < 6){
        if (rowI < 6){
          item += dnaReverse[rowI][colI]
        }
        rowI -= 1
        colI += 1
      }

      if (item.length >= 4 && item.search(regex) !== -1) {
        mutationExists = true
        break
      }
    }

    return mutationExists
  })
})
