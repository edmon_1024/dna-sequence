'use strict'

const fp = require('fastify-plugin')

module.exports = fp(async function (fastify, opts) {
  fastify.register(require('./fastify-firebase'), {
    name: "dna-sequence-api",
    cert: JSON.parse(process.env.FIREBASE_SERVICE_ACCOUNT_KEY),
  })
})
