'use strict'

module.exports = async function (fastify, opts) {
  const options = {
    schema: {
      body: {
        type: 'object',
        required: ['dna',],
        properties: {
          dna: {
            type: 'array',
            minItems: 6,
            maxItems: 6,
            items: {
              type: 'string',
              minLength: 6,
              maxLength: 6,
              pattern: "^[ATCG]{6}$"
            }
          },
        },
      }
    },
  }

  fastify.post('', options, async function (request, reply) {
    const { dna } = request.body
    const checkMutation = fastify.detectMutation(dna)

    const statsRef = await fastify.firebase.firestore().collection('stats')
    const save = await statsRef.doc(dna.join("")).set({ mutation: checkMutation })

    if (checkMutation) reply.send()
    reply.code(403).send()
  })
}
