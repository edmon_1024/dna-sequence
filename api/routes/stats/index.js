'use strict'

module.exports = async function (fastify, opts) {
  fastify.get('', async function (request, reply) {
    const statsRef = await fastify.firebase.firestore().collection('stats')
    const wMutation = await statsRef.where("mutation","==",true).get()
    const woMutation = await statsRef.where("mutation","==",false).get()

    const countWMutation = wMutation.size
    const countWoMutation = woMutation.size

    reply.send({
      count_mutations: countWMutation,
      count_no_mutation: countWoMutation,
      ratio: countWMutation/100,
    })
  })
}
